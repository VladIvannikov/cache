----------------------------------------------------
--�������� ������ ��� ������ ����� ����� ��� � ���������� ���� �� ���������� ������
----------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity CS_1 is
    Port ( tag : in  STD_LOGIC_VECTOR ( 6 downto 0);
           cache_addr : in  STD_LOGIC_VECTOR (2 downto 0);
           y : in  STD_LOGIC;
           data_in : in  STD_LOGIC_VECTOR (63 downto 0);
           data_out : out  STD_LOGIC_VECTOR (63 downto 0));
end CS_1;

architecture Behavioral of CS_1 is

	signal tmp : STD_LOGIC_VECTOR (71 downto 0);

begin

	lbl: for i in 0 to 7 generate
		-- tag
		tmp(6 + 7 * i downto 7 * i)
			<= tag when unsigned(cache_addr) = i
				else data_in(6 + 7 * i downto 7 * i);
		-- flag
		tmp(i + 7 * 8)
			<= '1' when unsigned(cache_addr) = i
				else data_in(i + 7 * 8);
	end generate lbl;

	data_out <= data_in when y = '0' else tmp;

end Behavioral;



