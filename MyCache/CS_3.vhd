-------------------------------------------------
--�������� ������ �� �������� ������ ��� ������ � ������ ��� ������ ������ � ���������� ���������� ����� �� ���� � �������� ������ 
-------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity CS_3 is
    Port ( data_mem : in  STD_LOGIC_VECTOR (63 downto 0);
           byte : in  STD_LOGIC_VECTOR (2 downto 0);
           data_in : in  STD_LOGIC_VECTOR (7 downto 0);
           data_cache : in  STD_LOGIC_VECTOR (63 downto 0);
           data_out : out  STD_LOGIC_VECTOR (63 downto 0);
           y : in  STD_LOGIC);
end CS_3;

architecture Behavioral of CS_3 is

	signal tmp : STD_LOGIC_VECTOR (63 downto 0);

begin
	
	lbl: for i in 0 to 7 generate
		tmp(6 + 7 * i downto 7 * i)
			<= data_in when unsigned(byte) = i
				else data_cache( + 7 * i downto 7 * i);
	end generate lbl;

	data_out <= data_mem when y = '0' else tmp;

end Behavioral;
