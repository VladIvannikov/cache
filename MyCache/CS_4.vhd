-----------------------------------------------
--����� ���������� ����� �� ������ ��� ������ ������
-----------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity CS_4 is
    Port ( data_cache : in  STD_LOGIC_VECTOR (63 downto 0);
           byte : in  STD_LOGIC_VECTOR (2 downto 0);
           data_out : out  STD_LOGIC_VECTOR (7 downto 0));
end CS_4;

architecture Behavioral of CS_4 is

begin
	
	with byte select data_out <=
		data_cache(7+8*0 downto 8*0) when "000",
		data_cache(7+8*1 downto 8*1) when "001",
		data_cache(7+8*2 downto 8*2) when "010",
		data_cache(7+8*3 downto 8*3) when "011",
		data_cache(7+8*4 downto 8*4) when "100",
		data_cache(7+8*5 downto 8*5) when "101",
		data_cache(7+8*6 downto 8*6) when "110",
		data_cache(7+8*7 downto 8*7) when "111",
	"0000000" when others;

end Behavioral;

