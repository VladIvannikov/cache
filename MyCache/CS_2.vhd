--------------------------------
--��������� ����� �� ������������ ������ �� ������� ����� � ��������� ������� ���������, ������� ������� ����� � ������ � ��������������� �����
--------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;


entity CS_2 is
    Port ( tag : in  STD_LOGIC_VECTOR (6 downto 0);
			  rnd : in  STD_LOGIC_VECTOR (2 downto 0); -- ��������� �������� LFRU
           data_in : in  STD_LOGIC_VECTOR (63 downto 0);
           F_Hit : out  STD_LOGIC;
           F_Emp : out  STD_LOGIC;
           cache_addr : out  STD_LOGIC_VECTOR (2 downto 0));
end CS_2;


architecture Behavioral of CS_2 is

	signal hit, empty : STD_LOGIC_VECTOR (7 downto 0);

begin

  -- Fill temp arrays (example:"0X00X00X")
	l_1: for i in 0 to 7 generate
	 -- Fill hit
		hit(i) <= '1'
			when tag = data_in(6 + 7 * i downto 7 * i) and data_in(i + 7 * 8) = '1'
			else '0';
	 -- Fill empty
		empty(i) <= '1'
			when data_in(i + 7 * 8) = '0'
			else '0';
	end generate l_1;
		
	cache_addr <=
	  -- Out HIT address
		"000" when hit = "00000001" else
		"001" when hit = "00000010" else
		"010" when hit = "00000100" else
		"011" when hit = "00001000" else
		"100" when hit = "00010000" else
		"101" when hit = "00100000" else
		"110" when hit = "01000000" else
		"111" when hit = "10000000" else
	  -- Out First Empty Address
		"000" when empty(0)='1' else
		"001" when (empty and "00000010") = "00000010" else
		"010" when (empty and "00000100") = "00000100" else
		"011" when (empty and "00001000") = "00001000" else
		"100" when (empty and "00010000") = "00010000" else
		"101" when (empty and "00100000") = "00100000" else
		"110" when (empty and "01000000") = "01000000" else
		"111" when (empty and "10000000") = "10000000" else
	  -- Out Random Address
		rnd when hit = "00000000" and empty = "00000000" else 
	"000";
	
  -- Out Flags
	F_Hit <= '1' when hit /= "00000000" else '0';
	F_Emp <= '1' when empty /= "00000000" else '0';

end Behavioral;

