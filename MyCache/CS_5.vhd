-----------------------------------------------
-- ������ �� ����� ��� ��� ��� �� ������ ���� �����
-----------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity CS_5 is
    Port ( tag : in  STD_LOGIC_VECTOR (6 downto 0);
           cch_row : in  STD_LOGIC_VECTOR (55 downto 0);
           addr : in  STD_LOGIC_VECTOR (2 downto 0);
           y : in  STD_LOGIC;
           tag_out : out  STD_LOGIC_VECTOR (6 downto 0));
end CS_5;

architecture Behavioral of CS_5 is

	signal result : STD_LOGIC_VECTOR (7 downto 0);

begin

	with addr select result <=
		cch_row(6+7*0 downto 7*0) when "000",
		cch_row(6+7*1 downto 7*1) when "001",
		cch_row(6+7*2 downto 7*2) when "010",
		cch_row(6+7*3 downto 7*3) when "011",
		cch_row(6+7*4 downto 7*4) when "100",
		cch_row(6+7*5 downto 7*5) when "101",
		cch_row(6+7*6 downto 7*6) when "110",
		cch_row(6+7*7 downto 7*7) when "111",
	"0000000" when others;

	tag_out <= tag when y = '0' else result;

end Behavioral;
