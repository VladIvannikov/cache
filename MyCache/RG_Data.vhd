-----------------------------------------------
-- ������� �������� 8-�� �������� ������� ������
-----------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- ������� �������� ������ (8 ��������)

entity RG_Data is
    Port ( data_in : in  STD_LOGIC_VECTOR (7 downto 0);
           ce : in  STD_LOGIC;
           clk : in  STD_LOGIC;
           data_out : out  STD_LOGIC_VECTOR (7 downto 0));
end RG_Data;

architecture Behavioral of RG_Data is

	signal tmp : STD_LOGIC_VECTOR (7 downto 0);

begin

	process (clk, ce) begin
		if clk'event and clk = '1' and ce = '1' then
			tmp <= data_in;
		end if;
	end process;

	data_out <= tmp;
	
end Behavioral;

