-----------------------------------------------
-- ������� �������� �������� ������ (13 ��������)
-----------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity RG_Addr is
    Port ( data_in : in  STD_LOGIC_VECTOR (12 downto 0);
           ce : in  STD_LOGIC;
           clk : in  STD_LOGIC;
           byte : out  STD_LOGIC_VECTOR (2 downto 0);
           page : out  STD_LOGIC_VECTOR (2 downto 0);
           tag : out  STD_LOGIC_VECTOR (6 downto 0));
end RG_Addr;

architecture Behavioral of RG_Addr is

	signal tmp : STD_LOGIC_VECTOR (12 downto 0);

begin

	process (clk, ce) begin
		if clk'event and clk = '1' and ce = '1' then
			tmp <= data_in;
		end if;
	end process;

	byte <= data_in(2 downto 0) when ce = '1' else tmp(2 downto 0);
	page <= data_in(5 downto 3) when ce = '1' else tmp(5 downto 3);
	tag <= data_in(12 downto 6) when ce = '1' else tmp(12 downto 6);

end Behavioral;

