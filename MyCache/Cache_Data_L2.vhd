-----------------------------------------------
-- ��� L2 ������ ������ 8���������, 64 ������ 
-----------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Cache_Data_L2 is
Port ( addr : in  STD_LOGIC_VECTOR (5 downto 0);
           data_in : in  STD_LOGIC_VECTOR (63 downto 0);
           clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           ce : in  STD_LOGIC;
           data_out : out  STD_LOGIC_VECTOR (63 downto 0));
end Cache_Data_L2;

architecture Behavioral of Cache_Data_L2 is
type storage is array (63 downto 0) of STD_LOGIC_VECTOR (63 downto 0);
	signal mem : storage;

begin
		process (clk) begin
		if clk'event and clk = '1' then
			if ce = '1' then
				mem(to_integer(unsigned(addr))) <= data_in;
			end if;
			if reset = '1' then
				for i in 0 to 63 loop
					mem(i) <= X"0000000000000000";
				end loop;
			end if;
		end if;
	end process;

	data_out <= mem(to_integer(unsigned(addr)));

end Behavioral;

